const express = require('express');

const mongoose = require('mongoose');

const app = express();

const port = 4000;

// Connecting MongoDB
mongoose.connect("mongodb+srv://fkbonacua8:admin@cluster0.xru7k.mongodb.net/session32?retryWrites=true&w=majority",
			{
				useNewUrlParser : true,
				useUnifiedTopology : true
			}
	);

	let db = mongoose.connection;
	db.on("error", console.error.bind(console, "There is an error with the connection"))
	db.once("open", () => console.log("Successfully connected to the database"));

// middleware
app.use(express.json())
app.use(express.urlencoded({
	extended: true}));

	// Mongoose Schemas
		// schema === blueprint of your data
	// const taskSchema = new mongoose.Schema({

	// 	name: String,
	// 	status: {
	// 		type: String,
	// 		default: 'pending'
	// 	}
	// });

	// activity - create a user schema
	const userSchema = new mongoose.Schema({

		email: String,
		username: String,
		password: String,
		age: Number,
		isAdmin: {
			type: Boolean,
			default: false
		}
	});

	// or test 

	const taskSchema = new mongoose.Schema({

		email: String,
		username: String,
		password: String,
		age: Number
		// status: {
		// 	type: String,
		// 	default: 'false'
		// }
		// isAdmin: {
		// 	type: Boolean,
		// 	default: false
		// }
	});

	// Model
	const Task = mongoose.model("Task", taskSchema);

	const User = mongoose.model("users", userSchema);
	// activity - create a User model.
	// const User = mongoose.model("User", userSchema);

	// Business Logic

	// Creating a new task
	// app.post("/users/signup", (req, res) => {

	// 	Task.findOne({email : req.body.email}, (err, result) => {

	// 		if (result != null && result.email === req.body.email) {
	// 			return res.send(`Duplicate task found`)
	// 		} else {
	// 			let newTask = new Task({
	// 				email: req.body.email
	// 			});

	// 			newTask.save((saveErr, savedTask) => {
	// 				if (saveErr) {
	// 					return console.error(saveErr)
	// 				} else {
	// 					return res.status(200).send(`New user created`)
	// 				}
	// 			});
	// 		}
	// 	});
	// });

// Retrieving all tasks
app.get("/tasks", (req, res) => {

	Task.find({}, (err, result) => {

		if (err){
			return console.log(err);
		} else {
			return res.status(200).json({
				tasks : result
			})
		}
	})
})

// Updating Task Name
app.put("/tasks/update/:taskId", (req, res) => {

	let taskId = req.params.taskId
	let name = req.body.name

	Task.findByIdAndUpdate(taskId, {name: name}, (err, updatedTask) => {
		if (err) {
			console.log(err)
		} else {
			res.send(`Congratulations the task has been updated`);
		}
	})
});

// Delete Task
app.delete("/tasks/archive-task/:taskId", (req, res) => {

	let taskId = req.params.taskId;

	Task.findByIdAndDelete(taskId, (err, deletedTask) => {
		if (err) {
			console.log(err)
		} else {
			res.send(`${deletedTask} has been deleted`)
		}
	})
});

// Activity

// 
let users = [
	{
		email: "cypresshill@mail.com",
		username: "beReal",
		password: "iv",
		isAdmin: false
	},
	{
		email: "wutangclan@mail.com",
		username: "methodMan",
		password: "Vvv",
		isAdmin: true
	},
	{
		email: "notorious@mail.com",
		username: "bigPoppa",
		password: "hypnotize",
		isAdmin: false
	},

];

// app.post('/users/signup', (req, res) => {

// 	console.log(req.body);

// 	let newUser = {

// 		email: req.body.email,
// 		username: req.body.username,
// 		password: req.body.password,
// 		isAdmin: req.body.isAdmin
// 	};

// 	users.push(newUser);
// 	console.log(users);

// 	res.send(`User ${req.body.username} has successfully registered`)
// });

app.post("users/signup", (req, res) => {
	User.findOne({users : req.body.users}, (err, result) => {
		if (result != null && result.users === req.body.users) {
			return res.send(`Error duplicate entry found: ${err}`)
		} else {
			let newUser = new User({
				users : req.body.users
			});
			newUser.save((saveErr, savedUser) => {
				if(saveErr){
					return console.error(saveErr)
				} else {
					return res.status(200).send(`New user registered: ${savedUser}`)
				};
			})
		}
	})
})



// Retrieving all tasks - activity
app.get("/users", (req, res) => {

	Task.find({}, (err, result) => {

		if (err){
			return console.log(err);
		} else {
			return res.status(200).json({
				tasks : result
			})
		}
	})
});

// endpoint /users/update-user/:wildcard
app.put("/users/update/:userId", (req, res) => {

	let taskId = req.params.taskId
	let username = req.body.username

	User.findByIdAndUpdate(taskId, {username: username}, (err, updatedTask) => {
		if (err) {
			console.log(err)
		} else {
			res.send(`Congratulations the user has been updated`);
		}
	})
});

// delete user
app.delete("/users/archive-user/:taskId", (req, res) => {

	let userId = req.params.userId;

	User.findByIdAndDelete(userId, (err, deletedUser) => {
		if (err) {
			console.log(err)
		} else {
			res.send(` User ${deletedUser} archived!`)
		}
	})
});


// ACTIVITY SOLUTION:
// schema
const userSchema = new mongoose.Schema({
	email: String,
	username: String,
	password: String,
	age: Number,
	status: {
		type: Boolean,
		default: false
	}
});

// model

const User = mongoose.model("User", userSchema);

// Bussines logic
app.post("/users/signup", (req, res) => {
	User.findOne({email: req.body.email}, (err, result) => {
		if (result != null && result.email == req.body.email) {
			return res.send(`User already exists`)
		}
	})

})

// retrieve all users
app.get("/users", (req, res) => {

	Task.find({}, (err, result) => {

		if (err){
			return console.log(err);
		} else {
			return res.status(200).json({
				tasks : result
			})
		}
	})
});
// Update task

app.listen(port, () => console.log(`Server running at port ${port}`));

